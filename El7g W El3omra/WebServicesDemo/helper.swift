//
//  helper.swift
//  El7g W El3omra
//
//  Created by ElMeMy on 6/5/18.
//  Copyright © 2018 ElMeMy. All rights reserved.
//

import UIKit

class helper: NSObject {

    class func restartApp()
    {
        guard let window = UIApplication.shared.keyWindow else{ return }
        let sb = UIStoryboard(name : "Main",bundle : nil)
        var vc: UIViewController
        if getApiToken() == nil {
            vc = sb.instantiateInitialViewController()!
        }else
        {
            vc = sb.instantiateViewController(withIdentifier: "main")

        }
        window.rootViewController = vc
        UIView.transition(with: window, duration: 0.5, options: .transitionFlipFromTop, animations: nil, completion: nil)
        
        
    }
    class func SaveApiToken(token: Int){
        let def = UserDefaults.standard
        def.setValue(token, forKey: "api_token")
        def.synchronize()
    }
    class func getApiToken() ->Int?{
        let def = UserDefaults.standard
        return def.object(forKey: "api_token") as? Int
    }
}
