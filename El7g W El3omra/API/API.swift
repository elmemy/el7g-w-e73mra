//
//  API.swift
//  El7g W El3omra
//
//  Created by ElMeMy on 6/5/18.
//  Copyright © 2018 ElMeMy. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class API: NSObject {

    class func login(phone : String,password : String, completion:@escaping (_ result: [String: AnyObject],_ success:Bool )->Void )
    {
        let url = URLS.login
        
        let parametter = [
            "phone" : phone,
            "password" : password,
            "lang" : "en"
        ]
        
        Alamofire.request(url, method: .post, parameters: parametter, encoding: URLEncoding.default, headers: nil)
            
            .validate(statusCode:200 ..< 300)
            .responseJSON{ response in
                switch response.result {
                case .failure(let error):
                    completion(error as! [String : AnyObject] ,false)
                case .success (let value):
                        completion(value as! [String: AnyObject],true)
                }
        }
    }
    class func GetCategories(cityId:Int,completion:@escaping (_ result: [String: AnyObject],_ success:Bool )->Void )
    {
        let url = URLS.GetCategories
        
        let parametter = [
            "city_id" : cityId,
            "lang" : "ar"
            ] as [String : Any]
        Alamofire.request(url, method: .post, parameters: parametter, encoding: URLEncoding.default, headers: nil)
            
            .validate(statusCode:200 ..< 300)
            .responseJSON{ response in
                switch response.result {
                case .failure(let error):
                    completion(error as! [String : AnyObject] ,false)
                case .success (let value):
                    completion(value as! [String: AnyObject],true)
                }
        }
    }
    
    class func editprofile(user_id : String , name : String,phone : String,password : String,email : String,nationality : String, completion:@escaping (_ result: [String: AnyObject],_ success:Bool )->Void )
    {
        let url = URLS.EditProfile
        
        let parametter = [
            "name" : name,
            "email" : email,
            "password" : password,
            "phone"     : phone,
            "nationality" : nationality,
            "user_id" : user_id

            ]
        
        Alamofire.request(url, method: .post, parameters: parametter, encoding: URLEncoding.default, headers: nil)
            
            .validate(statusCode:200 ..< 300)
            .responseJSON{ response in
                switch response.result {
                case .failure(let error):
                    completion(error as! [String : AnyObject] ,false)
                case .success (let value):
                    completion(value as! [String: AnyObject],true)
                }
        }
    }
    

    
    class func contact_us(title : String,message : String, completion:@escaping (_ result: [String: AnyObject],_ success:Bool )->Void )
    {
        let url = URLS.GetContact
        
        let parametter = [
            "title" : title,
            "message" : message,
        ]
        
        Alamofire.request(url, method: .post, parameters: parametter, encoding: URLEncoding.default, headers: nil)
            
            .validate(statusCode:200 ..< 300)
            .responseJSON{ response in
                switch response.result {
                case .failure(let error):
                    completion(error as! [String : AnyObject] ,false)
                case .success (let value):
                    completion(value as! [String: AnyObject],true)
                }
        }
    }
    //forget password
    class func forgotPassword(phone : String, completion:@escaping (_ result: [String: AnyObject],_ success:Bool )->Void )
    {
        let url = URLS.forgotPassword
        
        let parametter = [
            "phone" : phone,
        ]
        
        Alamofire.request(url, method: .post, parameters: parametter, encoding: URLEncoding.default, headers: nil)
            
            .validate(statusCode:200 ..< 300)
            .responseJSON{ response in
                switch response.result {
                case .failure(let error):
                    completion(error as! [String : AnyObject] ,false)
                case .success (let value):
                    completion(value as! [String: AnyObject],true)
                }
        }
    }
    
    class func resetPassword(user_id : String,password: String, completion:@escaping (_ result: [String: AnyObject],_ success:Bool )->Void )
    {
        let url = URLS.resetPassword
        
        let parametter = [
            "user_id" : user_id,
            "password" : password,
            ]
        
        Alamofire.request(url, method: .post, parameters: parametter, encoding: URLEncoding.default, headers: nil)
            
            .validate(statusCode:200 ..< 300)
            .responseJSON{ response in
                switch response.result {
                case .failure(let error):
                    completion(error as! [String : AnyObject] ,false)
                case .success (let value):
                    completion(value as! [String: AnyObject],true)
                }
        }
    }
    //AboutApp
    
    
    
    class func AboutApp( lang: String,completion:@escaping (_ result: [String: AnyObject],_ success:Bool )->Void )
    {
        let url = URLS.aboutapp
        
        let parametter = [
            "lang" : "ar",
            ]
        
        Alamofire.request(url, method: .post, parameters: parametter, encoding: URLEncoding.default, headers: nil)
            
            .validate(statusCode:200 ..< 300)
            .responseJSON{ response in
                switch response.result {
                case .failure(let error):
                    completion(error as! [String : AnyObject] ,false)
                case .success (let value):
                    completion(value as! [String: AnyObject],true)
                }
        }
    }
    
    //Terms
    
    
    
    class func TermsAds( lang: String,id_term : String ,completion:@escaping (_ result: [String: AnyObject],_ success:Bool )->Void )
    {
        let url = URLS.termsads
        
        let parametter = [
            "lang" : "ar",
            "id_term" : "1"
            ]
        
        Alamofire.request(url, method: .post, parameters: parametter, encoding: URLEncoding.default, headers: nil)
            
            .validate(statusCode:200 ..< 300)
            .responseJSON{ response in
                switch response.result {
                case .failure(let error):
                    completion(error as! [String : AnyObject] ,false)
                case .success (let value):
                    completion(value as! [String: AnyObject],true)
                }
        }
    }
    
    
    //AddADS
    
    
    
    class func AddADS( category_id: String,city_id : String,lat : String,lng :String,description: String , price:String,user_id: String,address :String  ,completion:@escaping(_ result: [String: AnyObject],_ success:Bool )->Void )
    {
        let url = URLS.AddADS
        
        let parametter = [
            "category_id" : "1",
            "city_id" : "2",
            "lat" : "26.820553",
            "city_id" : "30.802498",
            "description" :description,
            "price" : price,
            "address" : address,
            "img" : address,
            ]
        
        Alamofire.request(url, method: .post, parameters: parametter, encoding: URLEncoding.default, headers: nil)
            
            .validate(statusCode:200 ..< 300)
            .responseJSON{ response in
                switch response.result {
                case .failure(let error):
                    completion(error as! [String : AnyObject] ,false)
                case .success (let value):
                    completion(value as! [String: AnyObject],true)
                }
        }
    }
    
    //getCategories
    class func getCategories(city_id : String,lang : String, completion:@escaping (_ result: [String: AnyObject],_ success:Bool )->Void )
    {
        let url = URLS.getCategories
        
        let parametter = [
            "city_id" : "2",
            "lang" : "ar",
        ]
        
        Alamofire.request(url, method: .post, parameters: parametter, encoding: URLEncoding.default, headers: nil)
            
            .validate(statusCode:200 ..< 300)
            .responseJSON{ response in
                switch response.result {
                case .failure(let error):
                    completion(error as! [String : AnyObject] ,false)
                case .success (let value):
                    completion(value as! [String: AnyObject],true)
                }
        }
    }
    
    
    class func getRecent(category_id : String,lang : String, completion:@escaping (_ result: [String: AnyObject],_ success:Bool )->Void )
    {
        let url = URLS.getRecent
        
        let parametter = [
            "category_id" : category_id,
            "lang" : "ar",
            ]
        
        Alamofire.request(url, method: .post, parameters: parametter, encoding: URLEncoding.default, headers: nil)
            
            .validate(statusCode:200 ..< 300)
            .responseJSON{ response in
                switch response.result {
                case .failure(let error):
                    completion((error as? [String : AnyObject])! ,false)
                case .success (let value):
                    completion(value as! [String: AnyObject],true)
                }
        }
    }
    //getDetailsAd

    class func getDetailsAd(user_id : String,ad_id:String,lang : String, completion:@escaping (_ result: [String: AnyObject],_ success:Bool )->Void )
    {
        let url = URLS.getDetailsAd
        
        let parametter = [
            "user_id" : user_id,
            "lang" : "ar",
            "ad_id":ad_id
            ]
        
        Alamofire.request(url, method: .post, parameters: parametter, encoding: URLEncoding.default, headers: nil)
            
            .validate(statusCode:200 ..< 300)
            .responseJSON{ response in
                switch response.result {
                case .failure(let error):
                    completion((error as? [String : AnyObject])! ,false)
                case .success (let value):
                    completion(value as! [String: AnyObject],true)
                }
        }
    }
    
    
    
    class func getMyAds(user_id : String,lang : String, completion:@escaping (_ result: [String: AnyObject],_ success:Bool )->Void )
    {
        let url = URLS.getMyAds
        
        let parametter = [
            "user_id" : user_id,
            "lang" : "ar",
            ]
        
        Alamofire.request(url, method: .post, parameters: parametter, encoding: URLEncoding.default, headers: nil)
            
            .validate(statusCode:200 ..< 300)
            .responseJSON{ response in
                switch response.result {
                case .failure(let error):
                    completion((error as? [String : AnyObject])! ,false)
                case .success (let value):
                    completion(value as! [String: AnyObject],true)
                }
        }
    }
    
    class func getMyFavAds(user_id : String,lang : String, completion:@escaping (_ result: [String: AnyObject],_ success:Bool )->Void )
    {
        let url = URLS.getMyFavAds
        
        let parametter = [
            "user_id" : user_id,
            "lang" : "ar",
            ]
        
        Alamofire.request(url, method: .post, parameters: parametter, encoding: URLEncoding.default, headers: nil)
            
            .validate(statusCode:200 ..< 300)
            .responseJSON{ response in
                switch response.result {
                case .failure(let error):
                    completion((error as? [String : AnyObject])! ,false)
                case .success (let value):
                    completion(value as! [String: AnyObject],true)
                }
        }
    }
    
    
    
    
    
    
    class func getNearest(category_id : String,lang : String,user_id :String,lat:String,lng:String ,completion:@escaping (_ result: [String: AnyObject],_ success:Bool )->Void )
    {
        let url = URLS.getNearest
        
        let parametter = [
            "category_id" : category_id,
            "lng" : lng,
            "user_id" : user_id,
            "lat" : lat,
            "lang" : lang,
            ]
        
        Alamofire.request(url, method: .post, parameters: parametter, encoding: URLEncoding.default, headers: nil)
            
            .validate(statusCode:200 ..< 300)
            .responseJSON{ response in
                switch response.result {
                case .failure(let error):
                    completion((error as? [String : AnyObject])! ,false)
                case .success (let value):
                    completion(value as! [String: AnyObject],true)
                }
        }
    }
    
    
 
    
    class func getProfile(user_id : String, completion:@escaping (_ result: [String: AnyObject],_ success:Bool )->Void )
    {
        let url = URLS.GetProfile
        
        let parametter = [
            "user_id" : user_id
            ]
        
        Alamofire.request(url, method: .post, parameters: parametter, encoding: URLEncoding.default, headers: nil)
            
            .validate(statusCode:200 ..< 300)
            .responseJSON{ response in
                switch response.result {
                case .failure(let error):
                    completion(error as! [String : AnyObject]  ,false)
                case .success (let value):
                    print(value)
                    completion(value as! [String: AnyObject],true)
                }
        }
    }
}






