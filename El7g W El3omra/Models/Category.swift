//
//  Category.swift
//  El7g W El3omra
//
//  Created by ElMeMy on 6/24/18.
//  Copyright © 2018 ElMeMy. All rights reserved.
//

import Foundation

struct category{
    var id:Int
    var name:String
    var img:String
    
    init(id:Int,name:String,img:String) {
        self.id=id
        self.name=name
        self.img=img
    }
}
