//
//  Ads.swift
//  El7g W El3omra
//
//  Created by ElMeMy on 7/8/18.
//  Copyright © 2018 ElMeMy. All rights reserved.
//

import Foundation


struct Ads{
    var id:Int
    var name:String
    var img:String
    var cat:String
    init(id:Int,name:String,img:String,cat:String) {
        self.id=id
        self.name=name
        self.img=img
        self.cat=cat
    }
}


