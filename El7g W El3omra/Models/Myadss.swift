//
//  Myads.swift
//  El7g W El3omra
//
//  Created by ElMeMy on 7/15/18.
//  Copyright © 2018 ElMeMy. All rights reserved.
//

import Foundation

struct Myadss {
    private(set) public  var imageName : String
    private(set) public  var category : String
    private(set) public  var desc : String

    
    init(imageName: String,category: String,desc:String) {
        self.imageName = imageName
        self.category = category
        self.desc = desc
        
    }
}
