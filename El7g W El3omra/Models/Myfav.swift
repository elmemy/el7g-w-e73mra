//
//  Myfav.swift
//  El7g W El3omra
//
//  Created by ElMeMy on 7/10/18.
//  Copyright © 2018 ElMeMy. All rights reserved.
//

import Foundation

struct Myfav {
   private(set) public var title : String
   private(set) public  var imageName : String
   private(set) public  var category : String

    private(set) public  var maincategory : String

    init(title: String,imageName: String,category: String,maincategory:String) {
        self.title = title
        self.imageName = imageName
        self.category = category
        self.maincategory = maincategory


    }
}
