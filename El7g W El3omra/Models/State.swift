//
//  State.swift
//  El7g W El3omra
//
//  Created by ElMeMy on 8/11/18.
//  Copyright © 2018 ElMeMy. All rights reserved.
//

import Foundation
import GoogleMaps
import CoreLocation


struct State {
    let name: String
    let long: CLLocationDegrees
    let lat: CLLocationDegrees
    
    
    
    init(name: String,long: CLLocationDegrees,lat:CLLocationDegrees) {
        self.name = name
        self.long = long
        self.lat = lat
        
    }
}
