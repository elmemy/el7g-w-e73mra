//
//  Recent.swift
//  El7g W El3omra
//
//  Created by ElMeMy on 8/4/18.
//  Copyright © 2018 ElMeMy. All rights reserved.
//

import Foundation

struct RecentModel {
    private(set) public  var imageName : String
    private(set) public  var category : String
    private(set) public var desc : String
    private(set) public var price : String
    
    init(imageName: String,category: String,desc:String,price:String) {
        self.imageName = imageName
        self.category = category
        self.desc = desc
        self.price = price

    }
}
