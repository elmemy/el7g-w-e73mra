//
//  NearCell.swift
//  El7g W El3omra
//
//  Created by ElMeMy on 8/4/18.
//  Copyright © 2018 ElMeMy. All rights reserved.
//

import UIKit

class NearCell: UITableViewCell {

    @IBOutlet weak var AdsImg : UIImageView!
    @IBOutlet weak var category: UILabel!
    @IBOutlet weak var desc: UILabel!
    @IBOutlet weak var price: UILabel!

    func updateView(near : NearModel) {
        let url = URL(string: near.imageName)
        self.AdsImg.kf.setImage(with: url)
        category.text = near.category
        desc.text = near.desc
        price.text = near.price

    }


}
