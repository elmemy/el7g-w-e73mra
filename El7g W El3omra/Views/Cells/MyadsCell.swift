//
//  MyadsCell.swift
//  El7g W El3omra
//
//  Created by ElMeMy on 7/15/18.
//  Copyright © 2018 ElMeMy. All rights reserved.
//

import UIKit

class MyadsCell: UITableViewCell {

    @IBOutlet weak var AdsImg : UIImageView!
    @IBOutlet weak var category: UILabel!
    @IBOutlet weak var desc: UILabel!

    func updateView(myads : Myadss) {
        let url = URL(string: myads.imageName)
        self.AdsImg.kf.setImage(with: url)
        category.text = myads.category
        desc.text = myads.desc
        
    }

}
