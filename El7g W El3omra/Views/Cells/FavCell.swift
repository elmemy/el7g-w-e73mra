//
//  FavCell.swift
//  El7g W El3omra
//
//  Created by ElMeMy on 7/10/18.
//  Copyright © 2018 ElMeMy. All rights reserved.
//

import UIKit

class FavCell: UITableViewCell {

    @IBOutlet weak var AdsImg : UIImageView!
    @IBOutlet weak var Adstitle : UILabel!
    @IBOutlet weak var category: UILabel!
    @IBOutlet weak var maincategory: UILabel!
    
    func updateView(myfav : Myfav) {
        let url = URL(string: myfav.category)
        self.AdsImg.kf.setImage(with: url)
        Adstitle.text = myfav.title
        category.text = myfav.category
        maincategory.text = myfav.maincategory

    }

}
