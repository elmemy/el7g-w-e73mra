//
//  CategoryCell.swift
//  El7g W El3omra
//
//  Created by ElMeMy on 6/24/18.
//  Copyright © 2018 ElMeMy. All rights reserved.
//

import UIKit
import Kingfisher
class CategoryCell: UICollectionViewCell{

    @IBOutlet weak var picture: UIImageView!
    @IBOutlet weak var title: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configureCell(category: category){
        self.title.text = category.name
        let url = URL(string: category.img)
        self.picture.kf.setImage(with: url)
    }

}
