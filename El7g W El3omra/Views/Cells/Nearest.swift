//
//  Nearest.swift
//  El7g W El3omra
//
//  Created by ElMeMy on 8/3/18.
//  Copyright © 2018 ElMeMy. All rights reserved.
//

import UIKit

class NearestCell: UITableViewCell {

    @IBOutlet weak var AdsImg : UIImageView!
    @IBOutlet weak var category: UILabel!
    @IBOutlet weak var desc: UILabel!
    @IBOutlet weak var price: UILabel!

    func updateView(nearest : nearest) {
        let url = URL(string: nearest.imageName)
        self.AdsImg.kf.setImage(with: url)
        category.text = nearest.category
        desc.text = nearest.desc
        price.text = nearest.price

    }

}
