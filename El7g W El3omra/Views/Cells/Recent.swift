//
//  Recent.swift
//  El7g W El3omra
//
//  Created by ElMeMy on 8/4/18.
//  Copyright © 2018 ElMeMy. All rights reserved.
//

import UIKit

class Recent: UITableViewCell {

    @IBOutlet weak var AdsImg2 : UIImageView!
    @IBOutlet weak var category2: UILabel!
    @IBOutlet weak var desc2: UILabel!
    @IBOutlet weak var price2: UILabel!
    
    func updateView(recent : RecentModel) {
        let url = URL(string: recent.imageName)
        self.AdsImg2.kf.setImage(with: url)
        category2.text = recent.category
        desc2.text = recent.desc
        price2.text = recent.price
        
    }


}
