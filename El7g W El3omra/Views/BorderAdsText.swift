//
//  BorderAdsText.swift
//  El7g W El3omra
//
//  Created by ElMeMy on 6/2/18.
//  Copyright © 2018 ElMeMy. All rights reserved.
//

import UIKit

class BorderAdsText: UITextField {

    override func awakeFromNib() {
        super.awakeFromNib()
        layer.borderWidth = 1.0
        layer.borderColor = UIColor.blue.cgColor
    }

}
