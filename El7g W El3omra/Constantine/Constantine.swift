//
//  Constantine.swift
//  El7g W El3omra
//
//  Created by ElMeMy on 6/5/18.
//  Copyright © 2018 ElMeMy. All rights reserved.
//

import Foundation

struct URLS {
    static let main = "https://no3manpro.arabsdesign.com/El-hajj/api/"
    static let login = main + "login"
    static let GetCategories = main + "getCategory"
    static let GetContact = main + "contactUs"
    static let GetProfile = main + "getProfile"
    static let EditProfile = main + "postUpdateProfile"
    static let forgotPassword = main + "forgotPassword"
    static let resetPassword = main + "resetPassword"
    static let aboutapp = main + "aboutApp"
    static let termsads = main + "terms"
    static let AddADS = main + "postAds"
    static let getCategories = main + "getCategories"
    static let getMyFavAds = main + "MyFavourite"
    static let getMyAds = main + "MyAds"
    static let getNearest = main + "getNearest"
    static let getRecent = main + "getRecent"
    static let getDetailsAd = main + "getDetailsAd"




}
