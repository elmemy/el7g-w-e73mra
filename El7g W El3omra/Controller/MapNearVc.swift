//
//  MapNearVc.swift
//  El7g W El3omra
//
//  Created by ElMeMy on 8/10/18.
//  Copyright © 2018 ElMeMy. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation

class MapNearVc: UIViewController,CLLocationManagerDelegate {
    
//AIzaSyAUZul83g6x46_5sX3qR7QBo0376oehocQ
    
    var LocationManger = CLLocationManager()
    let authorizationStatus = CLLocationManager.authorizationStatus()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //            map()


        LocationManger.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled()
        {
            LocationManger.delegate = self
            LocationManger.desiredAccuracy = kCLLocationAccuracyBest
            LocationManger.startUpdatingLocation()
        }
        
        
        }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first
        {
//            print(location.coordinate.latitude)
            let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: 6.0)
            
//             let camera = GMSCameraPosition.camera(withLatitude: 31.0339726, longitude: 31.3601622, zoom:12.0)
            
            let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
            self.view = mapView
            
            
            let current_marker = GMSMarker()
            current_marker.position = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude:location.coordinate.longitude)
            current_marker.title = "Current Location"
            current_marker.snippet = "Hey, this is Current Location"
            current_marker.map = mapView
            
            

            
    //Api
            let lat = location.coordinate.latitude
            let lng = location.coordinate.longitude
 let id = UserDefaults.standard.value(forKey: "id") as? Int ?? 0
            API.getNearest(category_id: "\(id)", lang: "ar", user_id:"\(id)", lat: "\(lat)", lng: "\(lng)") { (value, success) in
                if success{
                    let dict = value
                    if let v = dict["value"] as? String{
                        switch v {
                        case  "0":
                            let alert = UIAlertController(title: "error", message: "Not Data", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: { (_) in
                                
                            }))
                            self.present(alert, animated: true, completion: nil)
                        case "1":
                            
                            if let data = dict["data"] as? [[String: Any]]{
//                                let Contact = aboutapp["content"] as? String
//                                self.Contact.text = Contact
                                
                                self.states=[]
                                //get data from JSON
                                for i in data{
                                    //                                let id = i["id"] as? Int ?? 0
                                    let category = i["category"] as? String ?? ""
                                    let lng = i["lng"] as? CLLocationDegrees ?? 0
                                    let lat = i["lat"] as? CLLocationDegrees ?? 0
                                    
                                    self.states.append(State(name: category, long: lng, lat: lat))
                                    
                                    let states = [
                                        State(name: category , long: lng, lat: lat)
                                    ]
                                    print (states)
                                    for state in states {
                                        let marker = GMSMarker()
                                        marker.position = CLLocationCoordinate2D(latitude: state.lat, longitude: state.long)
                                        marker.title = state.name
                                        marker.snippet = "Hey, this is \(state.name)"
                                        marker.map = mapView
                                    }

                                }
                                
                                
//
//                                            let states = [
//                                                State(name: "Alaska", long: 151.20, lat: -33.86),
//                                                State(name: "Alabama", long: 150.19, lat: -32.80),
//                                                // the other 51 states here...
//                                            ]
//                                let states = [State]()

                                
                                
                            }else{
                                //weak internet
                                let alert = UIAlertController(title: "error", message: "weak internet try again", preferredStyle: .alert)
                                alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: { (_) in
                                    
                                }))
                                self.present(alert, animated: true, completion: nil)
                                
                                
                            }
                        default : print(" ")
                        }
                    }
                    
                }
            }
    //endApi
        }
        

    }
    var states = [State]()


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func map ()
    {
//        let camera = GMSCameraPosition.camera(withLatitude: -33.86, longitude: 151.20, zoom: 6.0)

//        let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
//        self.view = mapView
        
        // Creates a marker in the center of the map.

//        let marker = GMSMarker()
//        marker.position = CLLocationCoordinate2D(latitude: -33.86, longitude: 151.20)
//        marker.title = "Sydney"
//        marker.snippet = "Australia"
//        marker.map = mapView
        
      
    }

   
}
