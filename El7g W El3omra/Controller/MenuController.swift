//
//  MenuController.swift
//  El7g W El3omra
//
//  Created by ElMeMy on 7/7/18.
//  Copyright © 2018 ElMeMy. All rights reserved.
//

import UIKit

class MenuController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

     
    }

    @IBAction func MainPressed(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "mainVc")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func ProfilePressed(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "profile")
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func MyadsPressed(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "Myads")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func FavPressed(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier:"Fav")
        self.navigationController?.pushViewController(vc, animated: true)

    }
    @IBAction func SettingPressed(_ sender: UIButton) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier:"Setting" )
        self.navigationController?.pushViewController(vc, animated: true)

    }
    @IBAction func CallPreesed(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "call")
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
}
