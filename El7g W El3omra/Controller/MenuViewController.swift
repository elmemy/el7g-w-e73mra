//
//  MenuViewController.swift
//  El7g W El3omra
//
//  Created by ElMeMy on 5/28/18.
//  Copyright © 2018 ElMeMy. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {

    @IBOutlet weak var city_id: UISwitch!
    @IBOutlet weak var categoriesCollectionView: UICollectionView!
    @IBOutlet weak var menuButton: UIBarButtonItem!
    override func viewDidLoad() {

        super.viewDidLoad()
//        sideMenu()
       

        // Do any additional setup after loading the view.
        categoriesCollectionView.delegate = self
        categoriesCollectionView.dataSource = self
    }
    @IBAction func cityChanged(_ sender: UISwitch) {
        loadCategories()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadCategories()
    }
    
    func loadCategories(){
        var cId = 1
        if city_id.isOn{
            cId = 1
        }else{
            cId = 2
        }
        API.GetCategories(cityId: cId) { (value, success) in
            if success{
                let dict = value
                if let v = dict["value"] as? String{
                    switch v {
                    case  "0":
                        let alert = UIAlertController(title: "error", message: "No Data", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: { (_) in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                    case "1":
                        if let data = dict["data"] as? [[String: Any]]{
                            self.categories=[]
                            //get data from JSON
                            for i in data{
                                let id = i["id"] as? Int ?? 0
                                let name = i["name"] as? String ?? ""
                                let image = i["image"] as? String ?? ""
                                self.categories.append(category(id: id, name: name, img: image))
                            }
                                
                            self.categoriesCollectionView.reloadData()
                            
                           
                            
                        }else{
                            //weak internet
                            let alert = UIAlertController(title: "error", message: "weak internet try again", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: { (_) in
                                
                            }))
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                    default : print(" ")
                    }
                }
                
            }
        }
    }
    
    var categories = [category]()

    
    func sideMenu(){
        revealViewController().rightViewRevealWidth = 300
        menuButton.target = revealViewController()
        menuButton.action = #selector(SWRevealViewController.rightRevealToggle(_:))
        view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
    }

}
extension MenuViewController: UICollectionViewDataSource, UICollectionViewDelegate{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCell", for: indexPath) as? CategoryCell else {return  UICollectionViewCell()}
        let category = categories[indexPath.row]
        cell.configureCell(category: category)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let category = categories[indexPath.row]
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        var vc = storyBoard.instantiateViewController(withIdentifier: "adscatt") as! adscatt
        
        vc.x = category.id
        
        

        self.present(vc, animated: true, completion: nil)

    }
    
}

