//
//  EditProfile.swift
//  El7g W El3omra
//
//  Created by ElMeMy on 6/30/18.
//  Copyright © 2018 ElMeMy. All rights reserved.
//

import UIKit
import Kingfisher
class EditProfile: UIViewController {

    @IBOutlet weak var nameLabel: UITextField!
    @IBOutlet weak var natLabel: UITextField!
    @IBOutlet weak var phonelabel: UITextField!
    @IBOutlet weak var emaillabel: UITextField!
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var passwordLabel: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let id = UserDefaults.standard.value(forKey: "id") as? Int ?? 0
        
        //        nameLabel.text = "\(id)"
        API.getProfile(user_id: "\(id)") { (value, success) in
            if success{
                let dict = value
                if let v = dict["value"] as? String{
                    switch v {
                    case  "0":
                        let alert = UIAlertController(title: "error", message: "No Data", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: { (_) in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                    case "1":
                        
                        if let userInfo = dict["data"] as? [String: Any]{
                            
                            let name = userInfo["name"] as? String
                            self.nameLabel.text = name
                            
                            let nat = userInfo["nationality"] as? String
                            self.natLabel.text = nat
                            
                           
                            let phone = userInfo["phone"] as? String
                            self.phonelabel.text = phone
                            
                            let email = userInfo["email"] as? String
                            self.emaillabel.text = email
                            
                            
                            let img = userInfo["avatar"] as? String
                            let url = URL(string: img!)
                            self.profileImg.kf.setImage(with: url)
                            
                    
                            
                        }else{
                            //weak internet
                            let alert = UIAlertController(title: "error", message: "weak internet try again", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: { (_) in
                                
                            }))
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                    default : print(" ")
                    }
                }
                
            }
        }
    }

    
    @IBAction func SaveEditProfile(_ sender: Any) {
        guard let name = nameLabel.text , !name.isEmpty else {return}
        guard let phone = phonelabel.text , !phone.isEmpty else {return}
        guard let email = emaillabel.text , !email.isEmpty else {return}
         let password = passwordLabel.text
        guard let nationality = natLabel.text , !nationality.isEmpty else {return}

        let id = UserDefaults.standard.value(forKey: "id") as? Int ?? 0

        API.editprofile(user_id: "\(id)" , name: name , phone: phone, password: password!,email:email,nationality:nationality) { (value, sucess) in
            if sucess{
                let dict = value
                if let v = dict["value"] as? String{
                    switch v {
                    case  "0":
                        let alert = UIAlertController(title: "error", message: "Login failed", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: { (_) in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                    case "1":
                        
                        if let userInfo = dict["data"] as? [String: Any]{
                            let def = UserDefaults.standard
                            //get data from JSON
                            let id = userInfo["id"] as? Int ?? 0
                            def.set(id, forKey: "id")
                            let name = userInfo["name"] as? String ?? ""
                            def.set(name, forKey: "name")
                            
                            
                            let phone = userInfo["phone"] as? String ?? ""
                            def.set(phone, forKey: "phone")
                            
                            
                            let email = userInfo["email"] as? String ?? ""
                            def.set(email, forKey: "email")
                            
                            
                            let nationality = userInfo["nationality"] as? String ?? ""
                            def.set(nationality, forKey: "nationality")
                       
                         
                            

                            //go to HomeVC
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyBoard.instantiateViewController(withIdentifier: "profile")
                            self.present(vc, animated: true, completion: nil)
                            
                        }else{
                            //weak internet
                            let alert = UIAlertController(title: "error", message: "weak internet try again", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: { (_) in
                                
                            }))
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                    default : print(" ")
                    }
                }
                
            }
            
        }
    }
    }



