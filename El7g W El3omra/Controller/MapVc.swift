//
//  MapVc.swift
//  El7g W El3omra
//
//  Created by ElMeMy on 8/10/18.
//  Copyright © 2018 ElMeMy. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
class MapVc: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    var LocationManger = CLLocationManager()
    let authorizationStatus = CLLocationManager.authorizationStatus()
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
        LocationManger.delegate = self
        ConfigureLocationServices()

        // Do any additional setup after loading the view.
    }

    @IBAction func CurrentLocation(_ sender: Any) {
    }
    
}
extension MapVc: MKMapViewDelegate
{
    
}

extension MapVc: CLLocationManagerDelegate
{
    func ConfigureLocationServices()
    {
        if authorizationStatus == .notDetermined
        {
            LocationManger.requestAlwaysAuthorization()
        }
        else
        {
            return
        }
    }

}

