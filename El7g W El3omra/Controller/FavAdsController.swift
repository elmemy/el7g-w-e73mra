//
//  FavAdsController.swift
//  El7g W El3omra
//
//  Created by ElMeMy on 7/10/18.
//  Copyright © 2018 ElMeMy. All rights reserved.
//

import UIKit

class FavAdsController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var categotyTable : UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
       categotyTable.dataSource = self
       categotyTable.delegate = self
        
        loadMyFavads()
        // Do any additional setup after loading the view.
    }

 
    func loadMyFavads(){
        let id = UserDefaults.standard.value(forKey: "id") as? Int ?? 0
        
        API.getMyFavAds(user_id: "\(id)",lang: "ar") { (value, success) in
            if success{
                let dict = value
                if let v = dict["value"] as? String{
                    switch v {
                    case  "0":
                        let alert = UIAlertController(title: "error", message: "No Data", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: { (_) in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                    case "1":
                        if let data = dict["data"] as? [[String: Any]]{
                            self.myfavads=[]
                            //get data from JSON
                            for i in data{
//                                let id = i["id"] as? Int ?? 0
                                let description = i["description"] as? String ?? ""
                                let image = i["image"] as? String ?? ""
                                let category = i["price"] as? String ?? ""
                                let maincategory = i["category"] as? String ?? ""

                                self.myfavads.append(Myfav(title: description, imageName: image, category: category, maincategory: maincategory))
                            }
                            
                            self.categotyTable.reloadData()
                            
                            
                            
                        }else{
                            //weak internet
                            let alert = UIAlertController(title: "error", message: "weak internet try again", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: { (_) in
                                
                            }))
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                    default : print(" ")
                    }
                }
                
            }
        }
    }
    
    var myfavads = [Myfav]()

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return DataService.instance.getMyFavAds().count
        return myfavads.count

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "FavCell") as? FavCell
        {
            let ads = myfavads[indexPath.row]
            cell.updateView(myfav: ads)
            return cell
        }
        else
        {
            return FavCell()
        }
        
        

    }
    
    
    
    
    

   

}
