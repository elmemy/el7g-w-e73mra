//
//  Myads.swift
//  El7g W El3omra
//
//  Created by ElMeMy on 7/15/18.
//  Copyright © 2018 ElMeMy. All rights reserved.
//

import UIKit

class Myads: UIViewController ,UITableViewDataSource,UITableViewDelegate{
    
    @IBOutlet weak var myads: UITableView!
    @IBOutlet weak var menuButton: UIBarButtonItem!

    override func viewDidLoad() {
        super.viewDidLoad()
        myads.dataSource = self
        myads.delegate = self
        loadMyAds()
        // Do any additional setup after loading the view.
    }
    
    
    func sideMenu(){
        revealViewController().rightViewRevealWidth = 300
        menuButton.target = revealViewController()
        menuButton.action = #selector(SWRevealViewController.rightRevealToggle(_:))
        view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
    }

    func loadMyAds(){
                let id = UserDefaults.standard.value(forKey: "id") as? Int ?? 0
        
        API.getMyAds(user_id: "\(id)",lang: "ar") { (value, success) in
            if success{
                let dict = value
                if let v = dict["value"] as? String{
                    switch v {
                    case  "0":
                        let alert = UIAlertController(title: "error", message: "No Data", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: { (_) in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                    case "1":
                        if let data = dict["data"] as? [[String: Any]]{
                            self.myadsss=[]
                            //get data from JSON
                            for i in data{
                                //                                let id = i["id"] as? Int ?? 0
                                let description = i["description"] as? String ?? ""
                                let image = i["image"] as? String ?? ""
                                let category = i["category"] as? String ?? ""
                              
                                self.myadsss.append(Myadss(imageName: image, category: category, desc: description))
                            }
                            
                            self.myads.reloadData()
                            
                            
                            
                        }else{
                            //weak internet
                            let alert = UIAlertController(title: "error", message: "weak internet try again", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: { (_) in
                                
                            }))
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                    default : print(" ")
                    }
                }
                
            }
        }
    }
    
    var myadsss = [Myadss]()
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //        return DataService.instance.getMyFavAds().count
        return myadsss.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "MyadsCell") as? MyadsCell
        {
            let ads = myadsss[indexPath.row]
            cell.updateView(myads: ads)
            return cell
            
        }
        else
        {
            return MyadsCell()
        }
        
        
        
    }
    
    
    
}
