//
//  ProfileController.swift
//  El7g W El3omra
//
//  Created by ElMeMy on 5/29/18.
//  Copyright © 2018 ElMeMy. All rights reserved.
//

import UIKit
import Kingfisher
class ProfileController: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var natLabel: UILabel!
    @IBOutlet weak var addresslabel: UILabel!
    @IBOutlet weak var phonelabel: UILabel!
    @IBOutlet weak var emaillabel: UILabel!
    @IBOutlet weak var address2label: UILabel!
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var menuButton: UIBarButtonItem!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let id = UserDefaults.standard.value(forKey: "id") as? Int ?? 0
        			
//        nameLabel.text = "\(id)"
        API.getProfile(user_id: "\(id)") { (value, success) in
            if success{
                let dict = value
                if let v = dict["value"] as? String{
                    switch v {
                    case  "0":
                        let alert = UIAlertController(title: "error", message: "No Data", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: { (_) in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                    case "1":
                        
                        if let userInfo = dict["data"] as? [String: Any]{
                            
                            let name = userInfo["name"] as? String
                            self.nameLabel.text = name
                            
                            let nat = userInfo["nationality"] as? String
                            self.natLabel.text = nat
                            
                            let address = userInfo["address"] as? String
                            self.addresslabel.text = address
                            
                            let phone = userInfo["phone"] as? String
                            self.phonelabel.text = phone
                            
                            let email = userInfo["email"] as? String
                            self.emaillabel.text = email
                           
                            
                            let address_2 = userInfo["address"] as? String
                            self.address2label.text = address_2
                            
                            
                            let img = userInfo["avatar"] as? String
                            let url = URL(string: img!)
                            self.profileImg.kf.setImage(with: url)
                            
                        }else{
                            //weak internet
                            let alert = UIAlertController(title: "error", message: "weak internet try again", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: { (_) in
                                
                            }))
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                    default : print(" ")
                    }
                }
                
            }
        }
    }
    func sideMenu(){
        revealViewController().rightViewRevealWidth = 300
        menuButton.target = revealViewController()
        menuButton.action = #selector(SWRevealViewController.rightRevealToggle(_:))
        view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
    }

    @IBAction func AddPressed(_ sender: Any) {
        performSegue(withIdentifier: "ok", sender: self)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 
    
    @IBAction func EditProfilePressed(_ sender: UIButton) {

        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "EditProfile")
        self.present(vc, animated: true, completion: nil)
        
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
