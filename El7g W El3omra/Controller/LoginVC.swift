//
//  LoginVC.swift
//  El7g W El3omra
//
//  Created by ElMeMy on 5/28/18.
//  Copyright © 2018 ElMeMy. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class LoginVC: UIViewController {
    
    @IBOutlet weak var PhoneTF: BorderTextField!
    @IBOutlet weak var PasswordTF: BorderTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func ForgetPress(_ sender: Any) {
        performSegue(withIdentifier: "Forget", sender: self)
    }
    @IBAction func LoginPressed(_ sender: Any) {
        guard let phone = PhoneTF.text , !phone.isEmpty else {return}
        guard let password = PasswordTF.text , !password.isEmpty else {return}
        
      
        API.login(phone: phone, password: password) { (value, sucess) in
            if sucess{
                let dict = value
                if let v = dict["value"] as? String{
                    switch v {
                    case  "0":
                        let alert = UIAlertController(title: "error", message: "Username or Password Incorect", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: { (_) in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                    case "1":
                        if let userInfo = dict["userInfo"] as? [String: Any]{
                            let def = UserDefaults.standard
                            //get data from JSON
                            let id = userInfo["id"] as? Int ?? 0
                            def.set(id, forKey: "id")
                            let name = userInfo["name"] as? String ?? ""
                            def.set(name, forKey: "name")
                            
                            //go to HomeVC
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyBoard.instantiateViewController(withIdentifier: "main")
                            self.present(vc, animated: true, completion: nil)

                        }else{
                            //weak internet
                            let alert = UIAlertController(title: "error", message: "weak internet try again", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: { (_) in
                                
                            }))
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                    default : print(" ")
                    }
                }
                
            }
        
        //=========
        }
    }
    
}
