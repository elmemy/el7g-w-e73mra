//
//  AdsFinalVc.swift
//  El7g W El3omra
//
//  Created by Aait.sa iPhone on 8/16/18.
//  Copyright © 2018 ElMeMy. All rights reserved.
//

import UIKit
import Kingfisher
class AdsFinalVc: UIViewController {
    @IBOutlet weak var Image: UIImageView!
    @IBOutlet weak var desc: UITextView!
    @IBOutlet weak var price: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let id = UserDefaults.standard.value(forKey: "id") as? Int ?? 0
        
        //        nameLabel.text = "\(id)"
        API.getDetailsAd(user_id: "1", ad_id:"192", lang: "ar") { (value, success) in
            if success{
                let dict = value
                if let v = dict["value"] as? String{
                    switch v {
                    case  "0":
                        let alert = UIAlertController(title: "error", message: "No Data", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: { (_) in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                    case "1":
                        
                        if let userInfo = dict["data"] as? [String: Any]{
                            
                            let name = userInfo["description"] as? String
                            self.desc.text = name
                            
                          
                          
                            let price = userInfo["price"] as? String
                            self.price.text = price
                            
                          
//
//                            let Image = userInfo["images"] as? String
//                            let url = URL(string: Image!)
//                            self.Image.kf.setImage(with: url)
                            
                        }else{
                            //weak internet
                            let alert = UIAlertController(title: "error", message: "weak internet try again", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: { (_) in
                                
                            }))
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                    default : print(" ")
                    }
                }
                
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
