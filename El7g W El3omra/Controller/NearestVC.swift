//
//  NearestVC.swift
//  El7g W El3omra
//
//  Created by ElMeMy on 8/3/18.
//  Copyright © 2018 ElMeMy. All rights reserved.
//

import UIKit
import CoreLocation

class NearestVC: UIViewController,UITableViewDataSource,UITableViewDelegate,CLLocationManagerDelegate {
    @IBOutlet weak var Nearest: UITableView!

    var LocationManger = CLLocationManager()
    let authorizationStatus = CLLocationManager.authorizationStatus()
    
    var lat = 0.0
    var lng = 0.0

    
    var cat_id = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        LocationManger.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled()
        {
            LocationManger.delegate = self
            LocationManger.desiredAccuracy = kCLLocationAccuracyBest
            LocationManger.startUpdatingLocation()
        }
        
        Nearest.dataSource = self
        Nearest.delegate = self
        
        loadNear()
        
        // Do any additional setup after loading the view.
    }
    
     func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.first
        lat = (location?.coordinate.latitude)!
        lng = (location?.coordinate.longitude)!
        }

    func loadNear(){
        let id = UserDefaults.standard.value(forKey: "id") as? Int ?? 0
    
         API.getNearest(category_id: "\(id)", lang: "ar", user_id:"\(id)", lat: "\(lat)", lng: "\(lng)") { (value, success) in
            
            if success{
                let dict = value
                if let v = dict["value"] as? String{
                    switch v {
                    case  "0":
                        let alert = UIAlertController(title: "error", message: "No Data", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: { (_) in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                    case "1":
                        if let data = dict["data"] as? [[String: Any]]{
                            self.mynear=[]
                            
                            //get data from JSON
                            for i in data{
                                //                                let id = i["id"] as? Int ?? 0
                                let description = i["description"] as? String ?? ""
                                let image = i["image"] as? String ?? ""
                                let category = i["category"] as? String ?? ""
                                let price = i["price"] as? String ?? ""
print ("\(i)")
                                self.mynear.append(NearModel(imageName: image, category: category, desc: description,price:price))
                            }
                            
                            self.Nearest.reloadData()
                            
                            
                            
                        }else{
                            //weak internet
                            let alert = UIAlertController(title: "error", message: "weak internet try again", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: { (_) in
                                
                            }))
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                    default : print(" ")
                    }
                }
                
            }
        }
    }
    
    var mynear = [NearModel]()
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //        return DataService.instance.getMyFavAds().count
        return mynear.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "NearCell") as? NearCell
        {
            let ads = mynear[indexPath.row]
            cell.updateView(near: ads)
            return cell
            
        }
        else
        {
            return NearCell()
        }
        
        
        
        
    }
    
    
    
}
