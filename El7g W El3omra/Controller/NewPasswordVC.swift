//
//  NewPasswordVC.swift
//  El7g W El3omra
//
//  Created by ElMeMy on 5/28/18.
//  Copyright © 2018 ElMeMy. All rights reserved.
//

import UIKit

class NewPasswordVC: UIViewController {

    @IBOutlet weak var password: BorderTextField!
    @IBOutlet weak var confirmpassword: BorderTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func NewPasswordPressed(_ sender: UIButton)
    {
        guard let password = password.text , !password.isEmpty else {return}
      
        
        
        let id = UserDefaults.standard.value(forKey: "id") as? Int ?? 0

        API.resetPassword(user_id: "\(id)",password: password)
        { (value, sucess) in
            if sucess
            {
                let dict = value
                if let v = dict["value"] as? String
                {
                    switch v
                    {
                    case  "0":
                        let alert = UIAlertController(title: "error", message: "Password Not Match", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: { (_) in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                    case "1":
                        
                        
                        let def = UserDefaults.standard
                        //get data from JSON
                        def.set(password, forKey: "password")
                        
                        let id = UserDefaults.standard.value(forKey: "user_id") as? Int ?? 0
                        def.set(id, forKey: "user_id")

                        
                        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                        let vc = storyBoard.instantiateViewController(withIdentifier: "main")
                        self.present(vc, animated: true, completion: nil)
                    default : print(" ")
                    }
                }
                
            }
            
        }
        
    }
    

    
  
}
