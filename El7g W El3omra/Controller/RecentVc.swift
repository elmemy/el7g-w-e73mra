//
//  RecentVc.swift
//  El7g W El3omra
//
//  Created by ElMeMy on 8/4/18.
//  Copyright © 2018 ElMeMy. All rights reserved.
//

import UIKit

class RecentVc: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var RecentTable : UITableView!
    var cat = 0
    var x = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        RecentTable.dataSource = self
        RecentTable.delegate = self

        loadRecent()
        // Do any additional setup after loading the view.
    }
    
    
    func loadRecent(){
//        let id = UserDefaults.standard.value(forKey: "id") as? Int ?? 0
        
        API.getRecent(category_id: "1",lang: "ar") { (value, success) in
            if success{
                let dict = value
                if let v = dict["value"] as? String{
                    switch v {
                    case  "0":
                        let alert = UIAlertController(title: "error", message: "No Data", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: { (_) in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                    case "1":
                        if let data = dict["data"] as? [[String: Any]]{
                            self.MyRecent=[]
                            //get data from JSON
                            for i in data{
                                //                                let id = i["id"] as? Int ?? 0
                                let description = i["description"] as? String ?? ""
                                let image = i["image"] as? String ?? ""
                                let category = i["category"] as? String ?? ""
                                let price = i["price"] as? String ?? ""
                                
                                self.MyRecent.append(RecentModel(imageName: image, category: category, desc: description,price:price))
                            }
                            
                            self.RecentTable.reloadData()
                            
                            
                            
                        }else{
                            //weak internet
                            let alert = UIAlertController(title: "error", message: "weak internet try again", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: { (_) in
                                
                            }))
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                    default : print(" ")
                    }
                }
                
            }
        }
    }
    
    var MyRecent = [RecentModel]()
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //        return DataService.instance.getMyFavAds().count
        return MyRecent.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "RecentCell") as? Recent
        {
            let recentt = MyRecent[indexPath.row]
            cell.updateView(recent: recentt)
            return cell
        }
        else
        {
            return Recent()
        }
        
        
        
    }
    
    
    
    
    
    
    


}
