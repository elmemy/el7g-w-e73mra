//
//  TermsController.swift
//  El7g W El3omra
//
//  Created by ElMeMy on 7/4/18.
//  Copyright © 2018 ElMeMy. All rights reserved.
//

import UIKit

class TermsController: UIViewController {

    @IBOutlet weak var TermsAds: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        API.TermsAds(lang: "ar" ,id_term: "1") { (value, success) in
            if success{
                let dict = value
                if let v = dict["value"] as? String{
                    switch v {
                    case  "0":
                        let alert = UIAlertController(title: "error", message: "No Data", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: { (_) in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                    case "1":
                        
                        if let terms = dict["data"] as? [String: Any]{
                            
                            let content = terms["content"] as? String
                            self.TermsAds.text = content
                            
                      
                            
                        }else{
                            //weak internet
                            let alert = UIAlertController(title: "error", message: "weak internet try again", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: { (_) in
                                
                            }))
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                    default : print(" ")
                    }
                }
                
            }
        }
        
    }

    @IBAction func NotAgree(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func Agree(_ sender: Any) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "AddAds")
        self.present(vc, animated: true, completion: nil)
    }
}
