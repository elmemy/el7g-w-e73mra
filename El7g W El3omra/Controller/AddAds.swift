//
//  AddAds.swift
//  El7g W El3omra
//
//  Created by ElMeMy on 7/4/18.
//  Copyright © 2018 ElMeMy. All rights reserved.
//

import UIKit
import Alamofire
class AddAds: UIViewController {
    
    
    
    private var dataSourece = ["","",""]
    @IBOutlet weak var ChooseCategories: UIPickerView!
    @IBOutlet weak var AddeessChoose: UIPickerView!
    @IBOutlet weak var Description: BorderAdsText!
    @IBOutlet weak var price: BorderAdsText!
    
    @IBOutlet weak var Categories: UIPickerView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ChooseCategories.delegate = self as UIPickerViewDelegate
        ChooseCategories.dataSource = self as UIPickerViewDataSource
        
        
        
        
        
        
        getCategory()
        
        
        
        
    }
    
    func getCategory(){
        
        
        API.getCategories(city_id: "2", lang: "ar") { (value, sucess) in
            if sucess{
                let dict = value
                if let v = dict["value"] as? String{
                    switch v {
                    case  "0":
                        let alert = UIAlertController(title: "error", message: "Username or Password Incorect", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: { (_) in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                    case "1":
                        if let cat = dict["data"] as? [[String: Any]]{
                            self.dataSourece = []
//                            let def = UserDefaults.standard
                            //get data from JSON
                            for c in cat{
                                let name = c["name"] as? String ?? ""
                                self.dataSourece.append(name)
                            }
                          
                            self.ChooseCategories.reloadAllComponents()
                        }else{
                            //weak internet
                            let alert = UIAlertController(title: "error", message: "weak internet try again", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: { (_) in
                                
                            }))
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                    default : print(" ")
                    }
                }
                
            }
            
            //=========
        }
    }


@IBAction func AddPressed(_ sender: UIButton) {
    
    
    guard let Description = Description.text , !Description.isEmpty else {return}
    guard let price = price.text , !price.isEmpty else {return}
    
    let id = UserDefaults.standard.value(forKey: "id") as? Int ?? 0
    
    API.AddADS(category_id: "1", city_id: "2", lat: "26.820553", lng: "30.802498", description: description, price: price, user_id: "\(id)", address: "Damietta"){ (value, sucess) in
        if sucess{
            let dict = value
            if let v = dict["value"] as? String{
                switch v {
                case  "0":
                    let alert = UIAlertController(title: "error", message: "Ads Not publish", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: { (_) in
                        
                    }))
                    self.present(alert, animated: true, completion: nil)
                case "1":
                    let def = UserDefaults.standard
                    //get data from JSON
                    def.set(Description, forKey: "Description")
                    def.set(price, forKey: "price")
                    
                    
                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyBoard.instantiateViewController(withIdentifier: "main")
                    self.present(vc, animated: true, completion: nil)
                    
                    
                    
                    
                    
                default : print(" ")
                }
            }
            
        }
        
    }
}


/*
 // MARK: - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
 // Get the new view controller using segue.destinationViewController.
 // Pass the selected object to the new view controller.
 }
 */

}

extension AddAds: UIPickerViewDelegate , UIPickerViewDataSource
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return dataSourece.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return dataSourece[row]
        
    }
}
