//
//  ContactUs.swift
//  El7g W El3omra
//
//  Created by ElMeMy on 6/24/18.
//  Copyright © 2018 ElMeMy. All rights reserved.
//

import UIKit

class ContactUs: UIViewController {
    
    @IBOutlet weak var titlemsg: UITextField!
    
    @IBOutlet weak var message: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    

    @IBAction func sentmessagepreesed(_ sender: Any) {
        guard let title = titlemsg.text , !title.isEmpty else {return}
        guard let message = message.text , !message.isEmpty else {return}
        
        API.contact_us(title: title,message: message)
        { (value, sucess) in
            if sucess{
                let dict = value
                if let v = dict["value"] as? String{
                    switch v {
                    case  "0":
                        let alert = UIAlertController(title: "error", message: "Message Not Send", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: { (_) in

                        }))
                        self.present(alert, animated: true, completion: nil)
                    case "1":
                            let def = UserDefaults.standard
                            //get data from JSON
                            def.set(title, forKey: "title")
                            def.set(message, forKey: "message")

//                            //go to HomeVC
//                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//                            let vc = storyBoard.instantiateViewController(withIdentifier: "main")
//                            self.present(vc, animated: true, completion: nil)

                            
                            let alert = UIAlertController(title: "Done", message: "Message has been Sent", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: { (_) in
                                
                            }))
                            self.present(alert, animated: true, completion: nil)
                            
                            
                            
                            
                       
                    default : print(" ")
                    }
                }

            }

        }
    }
        
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
