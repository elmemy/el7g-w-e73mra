//
//  adscat.swift
//  El7g W El3omra
//
//  Created by ElMeMy on 7/30/18.
//  Copyright © 2018 ElMeMy. All rights reserved.
//

import UIKit

class adscat: UIViewController {

    @IBOutlet weak var FirstView: UIView!
    @IBOutlet weak var SecondView: UIView!
    @IBOutlet weak var ThirdView: UIView!

    var x = 0
    
//     func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
//    {
//        if (segue.identifier == "RecentVc") {
//            let vc = segue.destination as! RecentVc
//            vc.cat_id = x
//            // Now you have a pointer to the child view controller.
//            // You can save the reference to it, or pass data to it.
//        }
//    }


    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "RecentVc" {
            let destination = segue.destination as! RecentVc
            destination.cat = x

        }
    }

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ThirdView.isHidden = true

        // Do any additional setup after loading the view.
    }



    @IBAction func showContainer(_ sender: UISegmentedControl) {

        switch sender.selectedSegmentIndex
        {

        case 0:
        // SHOWING THE HOME VIEW
        FirstView.alpha = 0
        SecondView.alpha = 0
        ThirdView.alpha = 1
        case 1:
            FirstView.alpha = 1
            SecondView.alpha = 0
            ThirdView.alpha = 0

        case 2:
            FirstView.alpha = 0
            SecondView.alpha = 1
            ThirdView.alpha = 0

        default:
            FirstView.alpha = 0
            SecondView.alpha = 0
            ThirdView.alpha = 0

        }
    }
 
}
